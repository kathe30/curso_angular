import {v4 as uuid} from 'uuid';

export class DestinoViaje {
    private selected: boolean;
    
    public servicios: string[];
    id = uuid();

    constructor(public nombre:string, public url:string, public votes: number = 0){
        this.servicios =['Agua', 'Desayuno'];
    }
    /* Metodo para indicar si esta seleccionado o no*/
    isSelected(): boolean {
        return this.selected;
    }
    /* Metodo para indicar si esta seleccionado o no*/
    setSelected(s:boolean) {
        this.selected = s
    }
    voteUp(){
        this.votes++;
    }
    voteDown(){
        this.votes--;
    }
}   