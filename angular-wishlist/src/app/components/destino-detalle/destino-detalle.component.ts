import { Component, OnInit} from '@angular/core';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { ActivatedRoute } from '@angular/router';
/*
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.module';
import { Injectable} from '@angular/core';

class DestinosApiClientViejo {
  getById(id: String): DestinoViaje {
    console.log('llamando por la clase vieja!');
    return null;
  }
}

interface AppConfig {
  apiEndpoint: String;
}
const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'mi_api.com'
};
const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

@Injectable()
class DestinosApiClientDecorated extends DestinosApiClient {
  constructor(@Inject(APP_CONFIG) private config: AppConfig, store: Store<AppState>) {
    super(store);
  }
  getById(id: String): DestinoViaje {
    console.log('llamando por la clase decorada!');
    console.log('config: ' + this.config.apiEndpoint);
    return super.getById(id);
  }
}
*/
@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [ DestinosApiClient
    //{ provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    //{ provide: DestinosApiClient, useClass: DestinosApiClientDecorated },
    //{ provide: DestinosApiClientViejo, useExisting: DestinosApiClient }
  ]
})

export class DestinoDetalleComponent implements OnInit {
  destino: DestinoViaje;
  style = {
    sources: {
      world: {
        type: 'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/mdestino/bd1d2e31-0ab4-474f-9785-3caa7c1d9d15aster/countries.geo.json'
      }
    },
    version: 8,
    layers: [{
      'id': 'countries',
      'type': 'fill',
      'source': 'world',
      'layout': {},
      'paint': {
        'fill-color': '#6F788A'
      }
    }]
  };
  
  constructor(private route: ActivatedRoute, private destinosApiClient:DestinosApiClient) { }
  //constructor(private route: ActivatedRoute, private destinosApiClient:DestinosApiClientViejo) { }
  ngOnInit(){
    const id = this.route.snapshot.paramMap.get('id');
    this.destino=this.destinosApiClient.getById(id); //this.destino = null; 
  }

}
